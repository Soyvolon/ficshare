Write-Host "Repository Renamer" -ForegroundColor Magenta;
Write-Host "";
Write-Host "========= README =========" -ForegroundColor Red;
Write-Host "Please ensure all work environements are closed before running this script (vscode, visual studio, etc.)." -ForegroundColor Red;
Write-Host "Please backup your repository. If this script fails, there is no way to recover any misalinged file names/data." -ForegroundColor Red;
Write-Host "========= README =========" -ForegroundColor Red;
Write-Host "";
Write-Host "-----" -ForegroundColor Magenta;

$newName = Read-Host -Prompt "Enter New Name";

$oldName = [System.IO.Path]::GetFileNameWithoutExtension((Get-ChildItem -Path .\ -Filter *.sln -Recurse -File)[0].Name);

Write-Host "-----" -ForegroundColor Magenta;
Write-Host "Replacing $($oldName) with $($newName)" -ForegroundColor Magenta;
Write-Host "-----" -ForegroundColor Magenta;

Function Replace {
    param(
        [Parameter(ValueFromPipeline = $true)]$o,
        [Parameter(ValueFromPipelineByPropertyName = $true)][string]$name
    )
    process {
        $newFileName = $o.Name.Replace($oldName, $newName);

        if ($newFileName -ne $o.Name) {
            $newFile = $o | Rename-Item -NewName { $newFileName } -PassThru -Force;

            if (($null -ne $newFile) -and (Test-Path -Path $newFile.FullName -PathType Leaf)) {
                $content = [System.IO.File]::ReadAllText($newFile.FullName);
                $newContent = $content.Replace($oldName, $newName);
    
                if ($content -ne $newContent) {
                    [System.IO.File]::WriteAllText($newFile.FullName, $newContent);
                    Write-Host "Changed $($name) to $($newFile.Name) within the file." -ForegroundColor Green;
                }
            }
            
            Write-Host "Renamed $($name) to $($newFile.Name)." -ForegroundColor Blue;
        } elseif (Test-Path -Path $o.FullName -PathType Leaf) {
            $content = [System.IO.File]::ReadAllText($o.FullName);
            $newContent = $content.Replace($oldName, $newName);

            if ($content -ne $newContent) {
                [System.IO.File]::WriteAllText($o.FullName, $newContent);
                Write-Host "Changed $($name) to $($o.Name) within the file." -ForegroundColor Green;
            }
        }
    }
}

$excludeFolders = @("node_modules", ".vs", ".git", ".vscode", "bin", "obj");
$excludeExtensions = @(".ps1", ".dll", ".gz", ".pdb");
# $excludeNameParts = @("runtimeconfig.json");

Write-Host "-----" -ForegroundColor Magenta;
Write-Host "Replacing files..." -ForegroundColor Magenta;
Write-Host "-----" -ForegroundColor Magenta;

# Do the files
Get-ChildItem $PSScriptRoot -Recurse -Directory     `
| Where-Object { 
    $valid = $true;
    for ($i = 0; $i -lt $excludeFolders.Count; $i++) {
        if ($_.FullName.Contains($excludeFolders[$i])) {
            $valid = $false;
            break;
        }
    }
    $valid;
} `
| ForEach-Object {
    $_ | Get-ChildItem -File                    `
    | Where-Object { -not ($_.Extension -in $excludeExtensions) }  `
    | Replace -name { $_.Name };
};

# Do the root folder
Get-ChildItem $PSScriptRoot -File `
    | Where-Object { -not ($_.Extension -in $excludeExtensions) } `
    | Replace -name { $_.Name };

Write-Host "-----" -ForegroundColor Magenta;
Write-Host "Replacing folders..." -ForegroundColor Magenta;
Write-Host "-----" -ForegroundColor Magenta;

# Then the folders
Get-ChildItem $PSScriptRoot -Recurse -Directory     `
| Where-Object { -not ($_.Name -in $excludeFolders) } `
| Replace -name { $_.Name };

# For debugging.
# | Select-Object -Skip 1 -First 1 `

Write-Host "-----" -ForegroundColor Magenta;
Write-Host "Renaming Complete." -ForegroundColor Magenta;
Write-Host "-----" -ForegroundColor Magenta;

Pause;