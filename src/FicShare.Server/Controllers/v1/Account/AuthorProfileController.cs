﻿using FicShare.Server.Services.Account;
using FicShare.Server.Services.Author;
using FicShare.Structures.Account;
using FicShare.Structures.Api;
using FicShare.Structures.Fanfiction.Author;
using FicShare.Structures.Http.Services;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FicShare.Server.Controllers.v1.Account;

/// <summary>
/// The controller for managing the author data for the current user.
/// </summary>
[Route($"{v1.VersionHelpers.BASE_ROUTE}/user")]
public class AuthorProfileController(
		IAuthorService authorService
	) : Controller
{

	/// <summary>
	/// Gets the userdata for the current user.
	/// </summary>
	/// <returns>JSON data for an action result holding a FicShareAccount</returns>
	[HttpGet("me/author")]
	[ProducesResponseType(StatusCodes.Status200OK, Type = typeof(AuthorProfile))]
	[ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ApiError))]
	[ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ApiError))]
	[Produces("application/json")]
	[APIReturnType(typeof(AuthorProfile))]
	[Authorize]
	public async Task<IActionResult> GetCurrentProfileAsync()
	{
		try
		{
			if (!VersionHelpers.TryGetAccountId(Request, out var accountId, out var error))
				return ResponseHelper.GetErrorResult(error);

			return await GetProfileAsync(accountId);
		}
		catch (Exception ex)
		{
			return ResponseHelper.GetErrorResult("An unexpected error occurred.", ex.Message);
		}
	}

	/// <summary>
	/// Gets the userdata of the specified user.
	/// </summary>
	/// <param name="userId">The user data to retrieve.</param>
	/// <returns>JSON data for an action result holding a FicShareAccount</returns>
	[HttpGet("{userId:guid}/author")]
	[ProducesResponseType(StatusCodes.Status200OK, Type = typeof(FicShareAccount))]
	[ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ApiError))]
	[ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ApiError))]
	[Produces("application/json")]
	[APIReturnType(typeof(FicShareAccount))]
	public async Task<IActionResult> GetProfileAsync(
		[FromRoute(Name = "userId")]
		Guid userId)
	{
		try
		{
			var accountRes = await authorService.GetAuthorProfileAsync(userId);

			return ResponseHelper.GetResult(accountRes);
		}
		catch (Exception ex)
		{
			return ResponseHelper.GetErrorResult("An unexpected error occurred.", ex.Message);
		}
	}

	/// <summary>
	/// Gets the userdata of the specified user.
	/// </summary>
	/// <param name="userSlug">The user data to retrieve.</param>
	/// <returns>JSON data for an action result holding a FicShareAccount</returns>
	[HttpGet("{userSlug}/author")]
	[ProducesResponseType(StatusCodes.Status200OK, Type = typeof(FicShareAccount))]
	[ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ApiError))]
	[Produces("application/json")]
	[APIReturnType(typeof(FicShareAccount))]
	public async Task<IActionResult> GetProfileAsync(
		[FromRoute(Name = "userSlug")]
		string userSlug)
	{
		try
		{
			var accountRes = await authorService.GetAuthorProfileAsync(userSlug);

			return ResponseHelper.GetResult(accountRes);
		}
		catch (Exception ex)
		{
			return ResponseHelper.GetErrorResult("An unexpected error occurred.", ex.Message);
		}
	}
}
