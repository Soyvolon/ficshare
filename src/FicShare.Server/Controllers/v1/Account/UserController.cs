using FicShare.Server.Services.Account;
using FicShare.Structures.Account;
using FicShare.Structures.Api;
using FicShare.Structures.Http.Services;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

using NuGet.Common;
using System.IdentityModel.Tokens.Jwt;

namespace FicShare.Server.Controllers.v1.Account;

/// <summary>
/// Provides endpoints for managing general user data.
/// </summary>
[Route($"{v1.VersionHelpers.BASE_ROUTE}/user")]
public class UserController(
        IAccountService accountService
    ) : Controller
{
    /// <summary>
    /// Gets the userdata for the current user.
    /// </summary>
    /// <returns>JSON data for an action result holding a FicShareAccount</returns>
    [HttpGet("me")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(FicShareAccount))]
	[ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ApiError))]
	[ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ApiError))]
    [Produces("application/json")]
    [APIReturnType(typeof(FicShareAccount))]
    [Authorize]
    public async Task<IActionResult> GetCurrentUserAsync()
    {
        try
        {
			if (!VersionHelpers.TryGetAccountId(Request, out var accountId, out var error))
				return ResponseHelper.GetErrorResult(error);

			return await GetUserAsync(accountId);
        }
        catch (Exception ex)
        {
            return ResponseHelper.GetErrorResult("An unexpected error occurred.", ex.Message);
        }
	}

	/// <summary>
	/// Gets the userdata of the specified user.
	/// </summary>
	/// <param name="userId">The user data to retrieve.</param>
	/// <returns>JSON data for an action result holding a FicShareAccount</returns>
	[HttpGet("{userId:guid}")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(FicShareAccount))]
	[ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ApiError))]
	[ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ApiError))]
    [Produces("application/json")]
    [APIReturnType(typeof(FicShareAccount))]
    public async Task<IActionResult> GetUserAsync(
        [FromRoute(Name = "userId")]
        Guid userId)
    {
        try
        {
			var accountRes = await accountService.GetAccountAsync(userId);
			if (accountRes.GetResult(out var err, out var data))
				return ResponseHelper.GetResult(StatusCodes.Status200OK, data);

			return ResponseHelper.GetErrorResult(err);
		}
		catch (Exception ex)
		{
			return ResponseHelper.GetErrorResult("An unexpected error occurred.", ex.Message);
		}
	}

	/// <summary>
	/// Gets the userdata of the specified user.
	/// </summary>
	/// <param name="userSlug">The user data to retrieve.</param>
	/// <returns>JSON data for an action result holding a FicShareAccount</returns>
	[HttpGet("{userSlug}")]
	[ProducesResponseType(StatusCodes.Status200OK, Type = typeof(FicShareAccount))]
	[ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(ApiError))]
	[ProducesResponseType(StatusCodes.Status400BadRequest, Type = typeof(ApiError))]
	[Produces("application/json")]
	[APIReturnType(typeof(FicShareAccount))]
	public async Task<IActionResult> GetUserAsync(
		[FromRoute(Name = "userSlug")]
		string userSlug)
	{
		try
		{
			var accountRes = await accountService.GetAccountAsync(userSlug);

			return ResponseHelper.GetResult(accountRes);
		}
		catch (Exception ex)
		{
			return ResponseHelper.GetErrorResult("An unexpected error occurred.", ex.Message);
		}
	}
}
