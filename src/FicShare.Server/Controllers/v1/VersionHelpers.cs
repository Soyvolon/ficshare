using System.Diagnostics.CodeAnalysis;
using System.IdentityModel.Tokens.Jwt;

namespace FicShare.Server.Controllers.v1;

internal static class VersionHelpers
{
    public const string VERSION = "v1";
    public const string BASE_ROUTE = "/api/" + VERSION;

    public static bool TryGetAccountId(HttpRequest request,
		[NotNullWhen(true)] 
		out Guid id,

		[NotNullWhen(false)]
		out string? error)
    {
		var authHeader = request.Headers.Authorization.First()?.Split(' ')[1];
		var tokenHandler = new JwtSecurityTokenHandler();
		var jwt = tokenHandler.ReadJwtToken(authHeader);

		var sub = jwt.Payload[JwtRegisteredClaimNames.Sub];
		if (sub is not string subVal || !Guid.TryParse(subVal, out var accountId))
		{
			id = default;
			error = "Subject value missing from auth token.";
			return false;
		}

		id = accountId;
		error = null;
		return true;
	}
}
