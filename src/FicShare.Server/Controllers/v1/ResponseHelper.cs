﻿using FicShare.Structures.Api;
using FicShare.Structures.Http.Services;
using FicShare.Structures.Result;

using Microsoft.AspNetCore.Mvc;

namespace FicShare.Server.Controllers.v1;

internal static class ResponseHelper
{
	internal static JsonResult GetErrorResult(params string[] errors)
		=> GetErrorResult(errorMessages: errors);

    internal static JsonResult GetErrorResult(IEnumerable<string> errorMessages, int statusCode = StatusCodes.Status400BadRequest)
		=> GetResult(statusCode, new ApiError()
		{
			Errors = errorMessages
		});

	internal static JsonResult GetResult<T>(int? statusCode, T? data)
        => new(data)
		{
			StatusCode = statusCode,
		};

	internal static JsonResult GetResult<T>(ActionResultHttp<T> result)
	{
		if (result.GetResult(out var err, out var res))
			return GetResult(result.StatusCode, res);

		return GetErrorResult(err, result.StatusCode);
	}
}
