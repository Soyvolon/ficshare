using System.Reflection;

namespace FicShare.Server;

public class Program
{
    public static void Main(string[] args)
        => CreateHostBuilder(args).Build().Run();

    public static IHostBuilder CreateHostBuilder(string[] args)
        => Host.CreateDefaultBuilder(args)
			.ConfigureAppConfiguration(config =>
			{
				var path = Path.Join(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location),
					"Config", "api_config.json");

				config.AddJsonFile(path);
			})
			.ConfigureWebHostDefaults(builder =>
            {
                builder.UseStartup<Startup>();
            });
}
