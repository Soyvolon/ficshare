using FicShare.Server.Services.Account;
using FicShare.Server.Services.Author;
using FicShare.Structures;
using FicShare.Structures.Api;
using FicShare.Structures.Database;
using FicShare.Structures.Http;

using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.IdentityModel.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;

using System.Collections.Concurrent;
using System.Net.Mime;
using System.Reflection;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace FicShare.Server;

public class Startup(IConfiguration configuration)
{
	private static ConcurrentDictionary<string, SecurityKey> BearerKeys { get; } = new();

	public IConfiguration Configuration { get; } = configuration;

	private readonly string[] xmlFilenames = [
		$"{Assembly.GetExecutingAssembly().GetName().Name}.xml",
		$"{Assembly.GetAssembly(typeof(EndpointData))?.GetName().Name}.xml"
	];

	public void ConfigureServices(IServiceCollection services)
	{
#if DEBUG
		IdentityModelEventSource.ShowPII = true;
#endif

		services.AddDbContextFactory<FicShareContext>(options =>
			options.UseNpgsql(
				Configuration.GetConnectionString("Primary")
			)
#if DEBUG
			.EnableSensitiveDataLogging()
			.EnableDetailedErrors()
#endif
			, ServiceLifetime.Singleton);

		services.AddScoped(p
			=> p.GetRequiredService<IDbContextFactory<FicShareContext>>().CreateDbContext());

		// Add services to the container.
		services.AddControllers()
			.AddJsonOptions(opts =>
			{
				var json = opts.JsonSerializerOptions;
				json.PropertyNameCaseInsensitive = true;
				json.TypeInfoResolver = new DataObjectPolymorphicTypeResolver();
				json.WriteIndented = false;
				json.PropertyNamingPolicy = JsonNamingPolicy.SnakeCaseLower;
				json.ReferenceHandler = ReferenceHandler.Preserve;
            });

		// For future reference: https://stackoverflow.com/a/73683912/11682098
		services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
			.AddJwtBearer(options =>
			{
				var keyCfg = Configuration.GetRequiredSection("Config");
				var authority = keyCfg["Authority"];
				var wellKnown = authority + "/.well-known/openid-configuration";

				// Then load the keys dynamically.
				JsonWebKey[] keys;
				using (var client = new HttpClient())
				{
					var wellKnownReq = new HttpRequestMessage(HttpMethod.Get, wellKnown);
					var res = client.Send(wellKnownReq);
					res.EnsureSuccessStatusCode();
					var type = new { jwks_uri = "" }.GetType();
					dynamic jwks = res.Content.ReadFromJsonAsync(type).Result
						?? throw new Exception("Failed to get JWKS url");

					var jwksReq = new HttpRequestMessage(HttpMethod.Get, jwks.jwks_uri);
					res = client.Send(jwksReq);
					res.EnsureSuccessStatusCode();
					type = new { keys = Array.Empty<JsonWebKey>() }.GetType();
					dynamic keyObj = res.Content.ReadFromJsonAsync(type).Result
						?? throw new Exception("Failed to get keys object");

					keys = (JsonWebKey[])keyObj.keys;
				}

				foreach (var key in keys)
				{
					if (!BearerKeys.TryAdd(key.KeyId, key))
						throw new Exception("Failed to add new public key. Make sure key ids are unique.");
				}

				options.TokenValidationParameters = new()
				{
					IssuerSigningKeys = BearerKeys.Values,

					ValidateLifetime = true,
					ValidateIssuerSigningKey = true,
					ValidateIssuer = true,
					ValidateAudience = true,

					ValidIssuer = keyCfg["Issuer"] ?? "",
					ValidAudience = keyCfg["Audience"] ?? "",

					IssuerSigningKeyResolver = (token, sec, kid, param) =>
					{
						if (BearerKeys.TryGetValue(kid, out var key))
							return [key];

						return [];
					}
				};

				options.Validate();

				options.Events = new()
				{
					OnChallenge = context =>
					{
						if (context is not JwtBearerChallengeContext ctx)
							return Task.CompletedTask;

						ctx.Response.OnStarting(async () =>
						{
							ctx.Response.ContentType = "application/json";
							await ctx.Response.WriteAsJsonAsync(new ApiError()
							{
								Errors = ["Unauthorized request."]
							});
						});

						return Task.CompletedTask;
					}
				};
			});

		// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
		services.AddEndpointsApiExplorer();
		services.AddSwaggerGen(options =>
		{
			options.SwaggerDoc("v1", new OpenApiInfo
			{
				Version = "v0.1.0",
				Title = "Web Server API",
				Description = "An API for the web server part of the app."
			});

			foreach (var xml in xmlFilenames)
				options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xml));
		});

		services.AddSingleton<IAccountService, AccountService>()
			.AddSingleton<IAuthorService, AuthorService>();
	}

	// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
	public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
	{
		Task.Run(async () =>
		{
			using var scope = app.ApplicationServices.CreateScope();
			var _logger = scope.ServiceProvider.GetRequiredService<ILogger<Startup>>();

			_logger.LogInformation("Running Startup Tasks...");
			_logger.LogInformation("Validating Database...");

			#region Database Validation
			var dbFac = scope.ServiceProvider.GetRequiredService<IDbContextFactory<FicShareContext>>();
			await using var db = await dbFac.CreateDbContextAsync();
			await ApplyDatabaseMigrationsAsync(db);
			#endregion

			_logger.LogInformation("... Startup Tasks Completed.");
		}).GetAwaiter().GetResult();

		// Configure the HTTP request pipeline.
		if (env.IsDevelopment() || env.IsProduction())
		{
			app.UseSwagger();
			app.UseSwaggerUI();
		}

		app.UseForwardedHeaders(new ForwardedHeadersOptions()
		{
			ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
		});

		app.UseRouting();

		app.UseAuthentication();
		app.UseAuthorization();

		app.UseEndpoints(endpoints =>
		{
			endpoints.MapControllers();
		});
	}

	private static async Task ApplyDatabaseMigrationsAsync(DbContext database)
	{
		try
		{
			if (!(await database.Database.GetPendingMigrationsAsync()).Any())
			{
				return;
			}

			await database.Database.MigrateAsync();
			await database.SaveChangesAsync();
		}
		catch
		{
			Console.WriteLine("[ERROR] Failed to get/update migrations.");
		}
	}

}
