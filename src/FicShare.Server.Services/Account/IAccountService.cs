﻿using FicShare.Structures.Account;
using FicShare.Structures.Result;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace FicShare.Server.Services.Account;
public interface IAccountService
{
	public Task<ActionResultHttp<FicShareAccount?>> GetAccountAsync(Guid oauthId);
	public Task<ActionResultHttp<FicShareAccount?>> GetAccountAsync(string userSlug);
}
