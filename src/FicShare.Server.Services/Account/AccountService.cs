﻿using FicShare.Structures.Account;
using FicShare.Structures.Database;
using FicShare.Structures.Result;

using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace FicShare.Server.Services.Account;
public class AccountService(
		IDbContextFactory<FicShareContext> dbContextFactory
	) : IAccountService
{
	/// <inheritdoc/>
	public async Task<ActionResultHttp<FicShareAccount?>> GetAccountAsync(Guid oauthId)
	{
		try
		{
			await using var dbContext = await dbContextFactory.CreateDbContextAsync();

			var account = await dbContext.Accounts
				.Where(e => e.AccountId == oauthId)
				.AsNoTrackingWithIdentityResolution()
				.FirstOrDefaultAsync();

			if (account is null)
			{
				account = new()
				{
					AccountId = oauthId
				};

				await dbContext.AddAsync(account);
				await dbContext.SaveChangesAsync();
			}

			return new(true, null, account);
		}
		catch (Exception ex)
		{
			return new(false, [$"An unexpected error occurred in {nameof(GetAccountAsync)}", ex.Message]);
		}
	}

	public async Task<ActionResultHttp<FicShareAccount?>> GetAccountAsync(string userSlug)
	{
		try
		{
			await using var dbContext = await dbContextFactory.CreateDbContextAsync();

			var account = await dbContext.Accounts
				.Where(e => e.AccountSlug == userSlug)
				.AsNoTrackingWithIdentityResolution()
				.FirstOrDefaultAsync();

			if (account is null)
				return new(false, ["The provided user slug does not match any existing user."],
					overrideStatusCode: StatusCodes.Status404NotFound);

			return new(true, null, account);
		}
		catch (Exception ex)
		{
			return new(false, [$"An unexpected error occurred in {nameof(GetAccountAsync)}", ex.Message]);
		}
	}
}
