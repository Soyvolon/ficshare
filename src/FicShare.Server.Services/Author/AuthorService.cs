﻿using FicShare.Structures.Database;
using FicShare.Structures.Fanfiction.Author;
using FicShare.Structures.Result;

using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace FicShare.Server.Services.Author;
public class AuthorService(
		IDbContextFactory<FicShareContext> dbContextFactory
	) : IAuthorService
{
	public async Task<ActionResultHttp<AuthorProfile?>> GetAuthorProfileAsync(Guid oauthId)
	{
		try
		{
			await using var dbContext = await dbContextFactory.CreateDbContextAsync();

			var account = await dbContext.Authors
				.Where(e => e.AccountKey == oauthId)
				.AsNoTrackingWithIdentityResolution()
				.FirstOrDefaultAsync();

			if (account is null)
				return new(false, ["No author data was found."],
					overrideStatusCode: StatusCodes.Status404NotFound);

			return new(true, null, account);
		}
		catch (Exception ex)
		{
			return new(false, [$"An unexpected error occurred in {nameof(GetAuthorProfileAsync)}", ex.Message]);
		}
	}

	public async Task<ActionResultHttp<AuthorProfile?>> GetAuthorProfileAsync(string userSlug)
	{
		try
		{
			await using var dbContext = await dbContextFactory.CreateDbContextAsync();

			var account = await dbContext.Authors
				.Include(e => e.Account)
				.Where(e => e.Account!.AccountSlug == userSlug)
				.AsNoTrackingWithIdentityResolution()
				.FirstOrDefaultAsync();

			if (account is null)
				return new(false, ["No author data was found."], 
					overrideStatusCode: StatusCodes.Status404NotFound);

			return new(true, null, account);
		}
		catch (Exception ex)
		{
			return new(false, [$"An unexpected error occurred in {nameof(GetAuthorProfileAsync)}", ex.Message]);
		}
	}
}
