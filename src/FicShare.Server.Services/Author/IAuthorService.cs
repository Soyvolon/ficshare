﻿using FicShare.Structures.Account;
using FicShare.Structures.Fanfiction.Author;
using FicShare.Structures.Result;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace FicShare.Server.Services.Author;
public interface IAuthorService
{
	public Task<ActionResultHttp<AuthorProfile?>> GetAuthorProfileAsync(Guid oauthId);
	public Task<ActionResultHttp<AuthorProfile?>> GetAuthorProfileAsync(string userSlug);
}
