using FicShare.Structures.Fanfiction.Author;
using FicShare.Structures.Fanfiction.Works;

namespace FicShare.Structures.Account;

/// <summary>
/// An account for the website.
/// </summary>
public class FicShareAccount : DataObject<Guid>
{
	/// <summary>
	/// The account ID from the oauth provider.
	/// </summary>
	public Guid AccountId { get; set; }

	/// <summary>
	/// The unique url part for this account.
	/// </summary>
	public string? AccountSlug { get; set; }

	/// <summary>
	/// The author profile for this account.
	/// </summary>
	public AuthorProfile? AuthorProfile { get; set; }
}
