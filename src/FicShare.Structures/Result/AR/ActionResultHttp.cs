﻿using Microsoft.AspNetCore.Http;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FicShare.Structures.Result;
/// <summary>
/// A special action result for handling http results.
/// </summary>
/// <typeparam name="T">The type of value that will be returned.</typeparam>
/// <remarks>
/// Create a new instance.
/// </remarks>
/// <param name="success">Sets <see cref="ActionResult.Success"/>.</param>
/// <param name="errors">Sets <see cref="ActionResult.Errors"/>.</param>
/// <param name="overrideStatusCode">The stauts code that should be used in response.</param>
public class ActionResultHttp<T>(bool success, List<string>? errors, T? result = default, int? overrideStatusCode = null)
	: ActionResult<T>(success, errors, result)
{
	/// <summary>
	/// The status code for this result.
	/// </summary>
	public int StatusCode { get; set; } = overrideStatusCode 
		?? (success ? StatusCodes.Status200OK : StatusCodes.Status500InternalServerError);
}
