namespace FicShare.Structures.Api;

/// <summary>
/// An error in the API.
/// </summary>
public class ApiError
{
    /// <summary>
    /// The error messages.
    /// </summary>
    public IEnumerable<string> Errors { get; set; } = [];
}

