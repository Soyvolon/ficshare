using FicShare.Structures.Account;
using FicShare.Structures.Fanfiction.Author;
using FicShare.Structures.Fanfiction.Location;
using FicShare.Structures.Fanfiction.Works;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

using System.Text.Json;

namespace FicShare.Structures.Database;

internal class FicShareDesignTimeContextFactory : IDesignTimeDbContextFactory<FicShareContext>
{
	public FicShareContext CreateDbContext(string[] args)
	{
		string cfgPath = Path.GetFullPath("../FicShare.Server/Config/api_config.json");
		var cfg = new ConfigurationBuilder()
			.AddJsonFile(cfgPath)
			.Build();
		
		var builder = new DbContextOptionsBuilder<FicShareContext>();
		builder.UseNpgsql(cfg.GetConnectionString("Primary"));

		return new FicShareContext(builder.Options);
	}
}

public class FicShareContext(DbContextOptions<FicShareContext> options) : DbContext(options)
{
    public DbSet<FicShareAccount> Accounts { get; protected set; }
	public DbSet<AuthorProfile> Authors { get; protected set; }
	public DbSet<WorkMetadata> Works { get; protected set; }
	public DbSet<WorkLocation> WorkLocations { get; protected set; }
	public DbSet<LocationMetadata> Locations { get; protected set; }

	/// <summary>
	/// Applies migrations to the database.
	/// </summary>
	/// <remarks>
	/// When using SQLite, this may have to be called twice,
	/// so we run the migration attempt twice.
	/// </remarks>
	/// <returns>A task for this action.</returns>
	public async Task ApplyMigrationsAsync()
    {
        for (int i = 0; i < 2; i++)
        {
            if (!(await Database.GetPendingMigrationsAsync()).Any())
            {
                return;
            }

            await Database.MigrateAsync();
            await SaveChangesAsync();
        }
    }

	protected override void OnModelCreating(ModelBuilder builder)
	{
		base.OnModelCreating(builder);

		builder.Entity<FicShareAccount>(ctx =>
		{
			ctx.HasKey(e => e.Key);
			ctx.HasIndex(e => e.AccountId);
			ctx.HasIndex(e => e.AccountSlug);

			ctx.HasOne(p => p.AuthorProfile)
				.WithOne(d => d.Account)
				.HasForeignKey<AuthorProfile>(d => d.AccountKey)
				.IsRequired(false);
		});

		builder.Entity<AuthorProfile>(ctx =>
		{
			ctx.HasKey(e => e.Key);

			ctx.HasMany(e => e.Works)
				.WithMany(e => e.Authors)
				.UsingEntity<AuthorWorkLink>(
					r => r.HasOne(d => d.Work)
						.WithMany(p => p.AuthorLinks)
						.HasForeignKey(d => d.WorkKey),
					l => l.HasOne(d => d.Author)
						.WithMany(p => p.WorkLinks)
						.HasForeignKey(d => d.AuthorKey)
				);
		});

		builder.Entity<AuthorWorkLink>(ctx =>
		{
			ctx.HasKey(e => e.Key);
		});

		builder.Entity<WorkMetadata>(ctx =>
		{
			ctx.HasKey(e => e.Key);

			ctx.HasMany(p => p.Locations)
				.WithOne(d => d.Work)
				.HasForeignKey(d => d.WorkKey);
		});

		builder.Entity<LocationMetadata>(ctx =>
		{
			ctx.HasKey(e => e.Key);

			ctx.HasMany(p => p.WorksInLocation)
				.WithOne(d => d.Location)
				.HasForeignKey(d => d.LocationKey);
		});

		builder.Entity<WorkLocation>(ctx =>
		{
			ctx.HasKey(e => e.Key);
		});
	}
}
