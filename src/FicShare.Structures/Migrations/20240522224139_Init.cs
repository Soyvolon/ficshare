﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace FicShare.Structures.Migrations
{
    /// <inheritdoc />
    public partial class Init : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Accounts",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uuid", nullable: false),
                    AccountId = table.Column<Guid>(type: "uuid", nullable: false),
                    AccountSlug = table.Column<string>(type: "text", nullable: true),
                    LastEdit = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Accounts", x => x.Key);
                });

            migrationBuilder.CreateTable(
                name: "Locations",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uuid", nullable: false),
                    BaseUrl = table.Column<string>(type: "text", nullable: false),
                    Title = table.Column<string>(type: "text", nullable: false),
                    Acronym = table.Column<string>(type: "text", nullable: false),
                    LastEdit = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Locations", x => x.Key);
                });

            migrationBuilder.CreateTable(
                name: "Works",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uuid", nullable: false),
                    Title = table.Column<string>(type: "text", nullable: false),
                    Description = table.Column<string>(type: "text", nullable: true),
                    LastEdit = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Works", x => x.Key);
                });

            migrationBuilder.CreateTable(
                name: "AuthorProfile",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uuid", nullable: false),
                    AccountKey = table.Column<Guid>(type: "uuid", nullable: true),
                    LastEdit = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AuthorProfile", x => x.Key);
                    table.ForeignKey(
                        name: "FK_AuthorProfile_Accounts_AccountKey",
                        column: x => x.AccountKey,
                        principalTable: "Accounts",
                        principalColumn: "Key");
                });

            migrationBuilder.CreateTable(
                name: "WorkLocations",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uuid", nullable: false),
                    WorkKey = table.Column<Guid>(type: "uuid", nullable: false),
                    LocationKey = table.Column<Guid>(type: "uuid", nullable: false),
                    RelativeUrl = table.Column<string>(type: "text", nullable: false),
                    LastEdit = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_WorkLocations", x => x.Key);
                    table.ForeignKey(
                        name: "FK_WorkLocations_Locations_LocationKey",
                        column: x => x.LocationKey,
                        principalTable: "Locations",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_WorkLocations_Works_WorkKey",
                        column: x => x.WorkKey,
                        principalTable: "Works",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "AuthorWorkLink",
                columns: table => new
                {
                    Key = table.Column<Guid>(type: "uuid", nullable: false),
                    AuthorKey = table.Column<Guid>(type: "uuid", nullable: false),
                    WorkKey = table.Column<Guid>(type: "uuid", nullable: false),
                    Public = table.Column<bool>(type: "boolean", nullable: false),
                    LastEdit = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_AuthorWorkLink", x => x.Key);
                    table.ForeignKey(
                        name: "FK_AuthorWorkLink_AuthorProfile_AuthorKey",
                        column: x => x.AuthorKey,
                        principalTable: "AuthorProfile",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_AuthorWorkLink_Works_WorkKey",
                        column: x => x.WorkKey,
                        principalTable: "Works",
                        principalColumn: "Key",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Accounts_AccountId",
                table: "Accounts",
                column: "AccountId");

            migrationBuilder.CreateIndex(
                name: "IX_Accounts_AccountSlug",
                table: "Accounts",
                column: "AccountSlug");

            migrationBuilder.CreateIndex(
                name: "IX_AuthorProfile_AccountKey",
                table: "AuthorProfile",
                column: "AccountKey",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_AuthorWorkLink_AuthorKey",
                table: "AuthorWorkLink",
                column: "AuthorKey");

            migrationBuilder.CreateIndex(
                name: "IX_AuthorWorkLink_WorkKey",
                table: "AuthorWorkLink",
                column: "WorkKey");

            migrationBuilder.CreateIndex(
                name: "IX_WorkLocations_LocationKey",
                table: "WorkLocations",
                column: "LocationKey");

            migrationBuilder.CreateIndex(
                name: "IX_WorkLocations_WorkKey",
                table: "WorkLocations",
                column: "WorkKey");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AuthorWorkLink");

            migrationBuilder.DropTable(
                name: "WorkLocations");

            migrationBuilder.DropTable(
                name: "AuthorProfile");

            migrationBuilder.DropTable(
                name: "Locations");

            migrationBuilder.DropTable(
                name: "Works");

            migrationBuilder.DropTable(
                name: "Accounts");
        }
    }
}
