﻿using FicShare.Structures.Account;
using FicShare.Structures.Fanfiction.Works;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FicShare.Structures.Fanfiction.Author;
/// <summary>
/// A link between an author and their works.
/// </summary>
/// <remarks>
/// This entity is used to control the data that is
/// displayed by this author.
/// </remarks>
public class AuthorWorkLink : DataObject<Guid>
{
	/// <summary>
	/// The physical account.
	/// </summary>
	public AuthorProfile? Author { get; set; }
	/// <summary>
	/// The id for the account.
	/// </summary>
	public Guid AuthorKey { get; set; }

	/// <summary>
	/// The work.
	/// </summary>
	public WorkMetadata? Work { get; set; }
	/// <summary>
	/// The id of the work object.
	/// </summary>
	public Guid WorkKey { get; set; }

	/// <summary>
	/// If true, this author will be visible on the list
	/// of authors for a work and the work will
	/// be in the list of an authors works.
	/// </summary>
	public bool Public { get; set; } = true;
}
