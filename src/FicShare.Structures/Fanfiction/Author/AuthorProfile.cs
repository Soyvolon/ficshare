﻿using FicShare.Structures.Account;
using FicShare.Structures.Fanfiction.Works;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FicShare.Structures.Fanfiction.Author;

/// <summary>
/// Author profile data.
/// </summary>
public class AuthorProfile : DataObject<Guid>
{
	/// <summary>
	/// The account that owns this author profile.
	/// </summary>
	public FicShareAccount? Account { get; set; }
	/// <summary>
	/// The key for the account object.
	/// </summary>
	public Guid? AccountKey { get; set; }

	/// <summary>
	/// The works that are written by this author.
	/// </summary>
	public List<WorkMetadata> Works { get; set; } = [];
	/// <summary>
	/// The links for the works written by this author.
	/// </summary>
	public List<AuthorWorkLink> WorkLinks { get; set; } = [];
}
