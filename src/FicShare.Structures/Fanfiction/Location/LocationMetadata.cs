﻿using FicShare.Structures.Fanfiction.Works;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FicShare.Structures.Fanfiction.Location;

/// <summary>
/// The metadata for a specific location.
/// </summary>
/// <remarks>
/// The location metadata is picked based on the base url of
/// the <see cref="Works.WorkLocation.FullUrl"/>.
/// </remarks>
public class LocationMetadata : DataObject<Guid>
{
	/// <summary>
	/// The URL of the website where works can be collected on.
	/// </summary>
	public required Uri BaseUrl { get; set; }
	/// <summary>
	/// The title of the source website where works can be collected on.
	/// </summary>
	public string Title { get; set; } = "";
	/// <summary>
	/// Some sites have a defined acronym (spacebattles = SB for example)
	/// while others may be randomly generated from the title of the site.
	/// </summary>
	public string Acronym { get; set; } = "";

	/// <summary>
	/// The list of works that are in this location.
	/// </summary>
	public List<WorkLocation> WorksInLocation { get; set; } = [];
}
