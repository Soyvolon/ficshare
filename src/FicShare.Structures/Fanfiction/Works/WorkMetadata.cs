﻿using FicShare.Structures.Account;
using FicShare.Structures.Fanfiction.Author;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FicShare.Structures.Fanfiction.Works;

/// <summary>
/// The metadata of a work.
/// </summary>
public class WorkMetadata : DataObject<Guid>
{
    /// <summary>
    /// The title of the work.
    /// </summary>
    public required string Title { get; set; }
    /// <summary>
    /// The description of the work.
    /// </summary>
    public string? Description { get; set; }

    /// <summary>
    /// The authors for this work.
    /// </summary>
    public List<AuthorProfile> Authors { get; set; } = [];
	/// <summary>
	/// The links for the authors of this work.
	/// </summary>
	public List<AuthorWorkLink> AuthorLinks { get; set; } = [];

    /// <summary>
    /// The locations this work can be found from.
    /// </summary>
    public List<WorkLocation> Locations { get; set; } = [];
}
