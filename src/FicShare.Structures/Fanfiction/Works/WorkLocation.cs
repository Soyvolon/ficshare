﻿using FicShare.Structures.Fanfiction.Location;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FicShare.Structures.Fanfiction.Works;

/// <summary>
/// An object representing the location of a work on the internet.
/// </summary>
public class WorkLocation : DataObject<Guid>
{
    /// <summary>
    /// The work this location represents.
    /// </summary>
    public WorkMetadata? Work { get; set; }
    /// <summary>
    /// The id of the work object.
    /// </summary>
    public Guid WorkKey { get; set; }

    /// <summary>
    /// The location this work is stored at.
    /// </summary>
    public LocationMetadata? Location { get; set; }
    /// <summary>
    /// The id of the location object.
    /// </summary>
    public Guid LocationKey { get; set; }

    /// <summary>
    /// The link to the first page of the work.
    /// </summary>
    public required Uri RelativeUrl { get; set; }
}
