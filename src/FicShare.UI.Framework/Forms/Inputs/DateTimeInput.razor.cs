using Microsoft.AspNetCore.Components;

namespace FicShare.UI.Framework.Forms.Inputs;
public partial class DateTimeInput : CustomInputBase<DateTime>
{
    [Parameter]
    public string Label { get; set; } = "";
}
