using Microsoft.AspNetCore.Components;

namespace FicShare.UI.Framework.Forms.Inputs;
public partial class InputGroup
{
    [Parameter, EditorRequired]
    public required string Label { get; set; }

    [Parameter, EditorRequired]
    public required RenderFragment ChildContent { get; set; }

    [Parameter, EditorRequired]
    public required string AdditionalCss { get; set; }
}
