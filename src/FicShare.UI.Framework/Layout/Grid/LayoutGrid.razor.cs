using Microsoft.AspNetCore.Components;

namespace FicShare.UI.Framework.Layout.Grid;
public partial class LayoutGrid
{
    [Parameter, EditorRequired]
    public required RenderFragment ChildContent { get; set; }
}
