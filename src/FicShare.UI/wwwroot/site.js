export class Util {
    static keyDownHandlers = {};
    static heartbeats = {};
    static heartbeatMethod = 'HeartbeatAsync';
    static devHeartbeat = false;

    /**
     * 
     * @returns {{width: number, height: number}} the window dimensions.
     */
    static getWindowDimensions() {
        return {
            width: window.innerWidth,
            height: window.innerHeight
        }
    }

    /**
     * 
     * @param {string} elementRef 
     * @returns {DOMRect?} bounding box.
     */
    static getBoundingBox(elementRef) {
        var element = document.querySelector(`[data-id="${elementRef}"]`);
        if (element) {
            return element.getBoundingClientRect();
        }

        return null;
    }

    /**
     * 
     * @param {any} dotNetRef 
     * @param {string} method 
     */
    static registerKeyDownHandler(dotNetRef, method) {
        let controller = new AbortController();

        let options = {
            signal: controller.signal
        };

        document.addEventListener('keydown', async (x) => {
            if (x.defaultPrevented || x.isComposing) {
                return;
            }

            if (!(dotNetRef in this.keyDownHandlers)) {
                controller.abort('Removed by client.');
                return;
            }

            await dotNetRef.invokeMethodAsync(method, {
                Code: x.code,
                ShiftKey: x.shiftKey,
                CtrlKey: x.ctrlKey,
                AltKey: x.altKey,
                MetaKey: x.metaKey
            });

            x.preventDefault();
        }, options)

        this.keyDownHandlers[dotNetRef] = controller;
    }

    /**
     * 
     * @param {any} dotNetRef 
     */
    static removeKeyDownHandler(dotNetRef) {
        if (dotNetRef in this.keyDownHandlers) {
            this.keyDownHandlers[dotNetRef].abort('Removed by server.');
            delete this.keyDownHandlers[dotNetRef];
        }
    }

    /**
     * 
     * @param {string} id 
     * @param {any} dotNetRef 
     * @param {method} destroy 
     */
    static addHeartbeat(id, dotNetRef, destroy) {
        this.heartbeats[id] = setInterval(() => this.heartbeat(id, dotNetRef, destroy), 2500);
    }

    /**
     * 
     * @param {string} id 
     * @param {any} dotNetRef 
     * @param {method} destroy 
     */
    static heartbeat(id, dotNetRef, destroy) {
        try {
            if (this.devHeartbeat) {
                console.log('heartbeat.');
                return;
            }

            dotNetRef.invokeMethodAsync(this.heartbeatMethod)
                .then((data) => {
                    if (!data) {
                        destroy(id);
                        clearInterval(this.heartbeats[id]);
                        delete this.heartbeats[id];
                    }
                }).catch(() => {
                    destroy(id);
                    clearInterval(this.heartbeats[id]);
                    delete this.heartbeats[id];
                });
        } catch (e) {
            destroy(id);
            clearInterval(this.heartbeats[id]);
            delete this.heartbeats[id];
        }
    }

    static triggerFileDownload(fileName, url) {
        const anchor = document.createElement('a');
        anchor.href = url;
        anchor.download = fileName ?? '';
        anchor.click();
        anchor.remove();
    }

    /**
     * Gets the current theme from local storage.
     */
    static getTheme() {
        const themes = ["dark", "light"]
        const theme = localStorage.theme || (window.matchMedia('(prefers-color-scheme: dark)').matches ? "dark" : "light");

        const wrapper = document.querySelectorAll('.ck-body-wrapper');
        wrapper.forEach(item => {
            item.classList.remove(...themes);
            item.classList.add(theme);
        });

        return theme;
    }

    /**
     * Saves the theme to local storage.
     * @param {string} theme The new theme.
     */
    static setTheme(theme) {
        if (theme) {
            localStorage.theme = theme;
        } else {
            localStorage.removeItem("theme");
        }
    }

    /**
     * Gets the scroll height of an element.
     * @param {string} id The id of the element (without a #)
     * @returns the scroll height.
     */
    static getScrollHeight(id) {
        const elem = document.getElementById(id);
        return elem.scrollHeight;
    }

    /**
     * Gets the transition duration of an element.
     * @param {string} id The id of the element (without a #)
     * @returns the transition duration.
     */
    static getTransitionDuration(id) {
        const elem = document.getElementById(id);
        const style = window.getComputedStyle(elem);
        return style.getPropertyValue('transition-duration')
    }
}

window.Util = Util;
