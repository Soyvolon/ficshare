using FicShare.UI.Services.Server.Controllers;
using FicShare.UI.Services.Server.Controllers.v1.Account;

using Microsoft.AspNetCore.Components;

using System.Text.Json;

namespace FicShare.UI.Pages;

public partial class Index
{
	[Inject]
	public IUserService UserService { get; set; }
	string Output { get; set; } = "No data.";

	protected async Task TestGetAccountAsync()
	{
		var res = await UserService.GetCurrentUserAsync();

		if (res.GetResult(out var err, out var data))
			Output = JsonSerializer.Serialize(data);
		else Output = "Errors: " + string.Join(", ", err);

		await InvokeAsync(StateHasChanged);
	}
}