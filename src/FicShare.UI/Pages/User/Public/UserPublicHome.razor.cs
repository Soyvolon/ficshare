﻿using FicShare.Structures.Account;
using FicShare.UI.Services.Server.Controllers.v1.Account;

using Microsoft.AspNetCore.Components;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FicShare.UI.Pages.User.Public;

/// <summary>
/// The public homepage of a user.
/// </summary>
public partial class UserPublicHome
{
    [Inject]
    public required IUserService UserService { get; set; }

    /// <summary>
    /// The oauth2 id of the account.
    /// </summary>
    [Parameter]
    public Guid? UserId { get; set; }
    /// <summary>
    /// The user slug that identifies this user (username-based-url path).
    /// </summary>
    [Parameter]
    public string? UserSlug { get; set; }

    private FicShareAccount Account { get; set; }

    protected override async Task OnParametersSetAsync()
    {
        await base.OnParametersSetAsync();

        if (UserId is not null)
            await LoadAccountByUserId(UserId.Value);
        else if (UserSlug is not null)
            await LoadAccountByUserSlug(UserSlug!);
        else
        {
            // TODO: redirect to current users page.
        }
    }

    private async Task LoadAccountByUserId(Guid userId)
    {
        // var res = await UserService.GetUserAsync();
    }

    private async Task LoadAccountByUserSlug(string userSlug)
    {

    }
}
