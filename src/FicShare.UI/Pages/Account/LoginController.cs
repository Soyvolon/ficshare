using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace FicShare.UI.Pages.Account;

[Authorize]
[Route("account")]
public class LoginController : Controller
{
    [Route("login")]
    [HttpGet]
    public Task<IActionResult> OnGetAsync(
        [FromQuery]
        string redirect = "/"
    )
    {
        return Task.FromResult<IActionResult>(Redirect(redirect));
    }

    /// <summary>
    /// Handles logout requests.
    /// </summary>
    /// <param name="redirect">The redirect URL.</param>
    /// <returns>The action result for this logout action.</returns>
    [Route("logout")]
    [HttpGet]
    public async Task<IActionResult> LogoutAsync()
    {
        await HttpContext.SignOutAsync();

        return Redirect("/");
    }
}