using System.Security.Claims;

namespace FicShare.UI.Services.Bus.Scoped;

/// <summary>
/// Template Description
/// </summary>
public partial interface IScopedDataBus
{
    public ClaimsPrincipal? CurrentUserClaims { get; set; }
}
