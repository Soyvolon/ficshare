using FicShare.Structures.Database;
using FicShare.Structures.Http.Services;
using FicShare.UI.Services.Alert;

using Cloudcrate.AspNetCore.Blazor.Browser.Storage;

using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using FicShare.UI.Services.Bus.Scoped;

namespace FicShare.UI.Services;

public static class ServiceLoader
{
    public static IServiceCollection LoadUIServices(this IServiceCollection collection, IConfiguration config)
    {
        collection.AddHttpClient(nameof(HTTPCallService), (services, client) =>
        {
            var cfg = services.GetRequiredService<IConfiguration>();
            var api = cfg["Api:BaseUri"] ?? "";

#if DEBUG
            var apiOverride = Environment.GetEnvironmentVariable("DEVVAR_APIOVERRIDE");
            if (apiOverride is not null)
                api = apiOverride;
#endif

            client.BaseAddress = new(api);
        });

        // setup the templating context for linking things together
        collection.AddDbContextFactory<FicShareContext>(options =>
        {
            options.UseSqlite(config.GetConnectionString("COAPIContext"))
                .EnableSensitiveDataLogging()
                .EnableDetailedErrors();
        });

        collection.AddScoped<IAlertService, AlertService>()
            .AddScoped<IScopedDataBus, ScopedDataBus>();

        collection.LoadBaseServices();

        collection.AddStorage();

        return collection;
    }
}
