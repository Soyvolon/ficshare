namespace FicShare.UI.Services.CodeGenerator;

internal class Program
{
    static void Main(string[] args)
    {
        ApiServiceGenerator.Run(args);
    }
}
